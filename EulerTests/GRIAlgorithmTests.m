//
//  GRIAlgorithmTests.m
//  Euler
//
//  Created by Brian on 6/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "GSAlgorithm.h"

@interface GRIAlgorithmTests : XCTestCase

@end

@implementation GRIAlgorithmTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSequenceSum {
    // This is an example of a functional test case.
    XCTAssertEqual([GSAlgorithm sequenceSumTo:2] , 3 ,@"Sum of sequence fail - sum to 2" );
    XCTAssertEqual([GSAlgorithm sequenceSumTo:3] , 6 ,@"Sum of sequence fail - sum to 3" );
}


@end
