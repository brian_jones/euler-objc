[![Build Status](https://travis-ci.org/grimley517/euler-objC.svg?branch=master)](https://travis-ci.org/grimley517/euler-objC)

#Euler solutions in objective C


Solutions to the Project Euler problems in Objective C


https://projecteuler.net/problems

## Problem 1

https://projecteuler.net/problem=1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

This was solved by using an implementation of the sum of a sequence, and adjusting this by multiplying the sequence to find the sum of multiples.

