//
//  GSAlgorithm.h
//  Euler
//
//  Created by Brian on 6/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//
//  This is a general class containing a number of General Algorithms.

#import <Foundation/Foundation.h>

@interface GSAlgorithm : NSObject

+(int)sequenceSumTo: (int)n;

@end
