//
//  p1.m
//  Euler
//
//  Created by Brian on 5/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import "p1.h"


@implementation p1

@synthesize target, total;

+ (id)initForProblem
{
    p1 *me = [[p1 alloc] initWithTarget:1000];
    return me;
}


-(id)initWithTarget:(int)withTarget
{
    self.target = withTarget-1;// as we want those less than the target
    self.total = [self sumto];
    return self;
}

- (int) sequences:(int)n
{
    int temp = self.target /n;
    return (n * [GSAlgorithm sequenceSumTo:temp]);
}

- (int) sumto
{
                 
    return([self sequences:5]+[self sequences:3]-[self sequences:15]);
}

@end
