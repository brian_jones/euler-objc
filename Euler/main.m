//
//  main.m
//  Euler
//
//  Created by Brian on 5/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "p1.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        int code = (int)argv[1];
        switch (code){
            case 0:{
                NSLog(@"use 'euler n' to see the answer to problem n");
            }
            case 1:{
                p1* me = [p1 initForProblem];
                NSLog(@"%d",[me total]);
                break;
            }
            default:{
                NSLog (@"Problem Not solved ...yet \n use euler n to get the answer to problem n");
                return 1;
            }
        }
    }
    return 0;
}
