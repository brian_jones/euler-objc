//
//  GSAlgorithm.m
//  Euler
//
//  Created by Brian on 6/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import "GSAlgorithm.h"

@implementation GSAlgorithm

+(int)sequenceSumTo: (int)n

{
    float left = n/2.0;//this needs to be 2.0 not 2 as dividing by an int gives the wrong result.
    float right = n+1;
    float ans = left*right;
    return (int) ans;
    
}

@end
