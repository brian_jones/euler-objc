//
//  p1.h
//  Euler
//
//  Created by Brian on 5/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GSAlgorithm.h"

@interface p1 : NSObject

@property int target;
@property int total;

- (id)initWithTarget: (int) withTarget;

+ (id)initForProblem;


@end
