//
//  problem1tests.m
//  Euler
//
//  Created by Brian on 5/10/2014.
//  Copyright (c) 2014 Grimsoft. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "p1.h"

@interface problem1tests : XCTestCase

@end

@implementation problem1tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test1 {
    // as stated in the question the sum of mults to 10 is 23
    p1 *test = [[p1 alloc] initWithTarget:10];
    XCTAssertEqual([test total], 23,@"p1 - input of 10 does not yeild 23");
}



@end
